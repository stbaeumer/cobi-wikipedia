var request = require("request")
var express = require('express');
var bodyParser = require('body-parser')
var app = express();

class Item{
    constructor(id, title, description, imageUrl, latitude, longitude, url, category, distanceInKm, aclass, icon){
        this.id = id       
        this.title = title 
        this.description = description     
        this.imageUrl = imageUrl
        this.latitude = latitude
        this.longitude = longitude
        this.url = url
        this.category = category
        this.distanceInKm = distanceInKm
        this.aclass = aclass
        this.icon = icon
        this.items = []
    }
}

// standard coordinates, if none delivered

let latitude = ""//51.94583
let longitude = ""//7.1675

app.use(bodyParser.urlencoded({extended: true}))

app.use(express.static('public'));

app.set('view engine', 'ejs')

// 1st layer menue items

let items = []

items.push(new Item("0", "Interessantes in der Umgebung anzeigen", "Interessantes in der Umgebung anzeigen", null, null, null, "?id=0&latitude=&longitude=&layer=2", null, null,"",""))
items.push(new Item("1", "Bahnhöfe in der Umgebung suchen", "Bahnhöfe in der Umgebung suchen", null, null, null, "?id=1&latitude=&longitude=&layer=2", null, null,"",""))
items.push(new Item("2", "Suchbegriff eingeben", "Suchbegriff eingeben", null, null, null, "?id=2&latitude=&longitude=&layer=2", null, null,"",""))

app.get('/', (req, res) => {
    
    let headline = "Eine Wikipedia-Aktion wählen."

    if(!req.query.layer) res.render('index',{items:items, headline:headline, headlineImgUrl:null, layer:1})    

    // 2nd layer presents wikipedida-results

    if(req.query.layer == 2){

        if(req.query.id == 2){

            headline = "Wikipedia durchsuchen"
            res.render('index',{items:null, headline:headline, headlineImgUrl:null, layer:2})    
        }  

        latitude = (req.query.latitude != null ? req.query.latitude : null);
        longitude = (req.query.longitude != null ? req.query.longitude : null)
        
        headline = "Auswählen ..."

        // Don't search wikipedia, if no latitide or longitude is present and don't search wikipedia if results already captured

        if(!latitude || !longitude){
            erroritems = []
            erroritems.push(new Item(0,"","Längengrad: " + latitude,"","","","?","","","",""))
            erroritems.push(new Item(1,"","Breitengrad: " + longitude,"","","","?","","","",""))
            erroritems.push(new Item(1,"","Klicken Sie, um zur Startseite zurückzukehren ..." + longitude,"","","","?","","","",""))
            res.render('index',{items:erroritems, headline:"Hoppla! Es gibt nichts anzuzeigen, weil Längen- und Breitengrad nicht ausgelesen werden können.", headlineImgUrl:null, layer:2})                
        } 
        
        // remove items before adding new items 

        items[0].items = []
        items[1].items = []

        let url = "https://de.wikipedia.org/w/api.php?format=json&action=query&prop=coordinates%7Cpageimages%7Cpageterms&colimit=50&piprop=thumbnail&pithumbsize=144&pilimit=50&wbptterms=description&generator=geosearch&ggscoord=" + latitude + "%7C" + longitude + "&ggsradius=10000&ggslimit=50";
        
        request({
            url: url,
            json: true
        }, function (error, response, body) {
        
            if (!error && response.statusCode === 200 && body.query != undefined) {

                var locations = body.query.pages
                
                let i = 0
                let y = 0
                
                for (const location of Object.keys(locations)) {
                    
                    var item = new Item();
                    
                    item.id = i                    
                    item.title = locations[location].title         
                    item.description = locations[location].title
                    item.descriptionUrl = "https://de.m.wikipedia.org//wiki/" + item.title

                    for (const source of Object.keys(locations[location])) {                    
                        if(typeof locations[location][source].source != "undefined"){
                            item.imageUrl = locations[location][source].source                        
                        }
                    }
    
                    // ignore wikipedia-results, that don't come with an image

                    if(item.imageUrl != null){

                        for (const coord of Object.keys(locations[location].coordinates)) { 
                            item.latitude = locations[location].coordinates[coord].lat
                            item.longitude = locations[location].coordinates[coord].lon
                        }
                        
                        item.distanceInKm = parseFloat(Math.round(getDistanceFromLatLonInKm(latitude,longitude,item.latitude,item.longitude) * 100) / 100).toFixed(2) 
                    
                        item.description = item.title + " ( ~" + item.distanceInKm + " km )" 
     
                        // define operations for every wikipedia-result 

                        item.items.push(new Item(1, "Mehr Infos", "Mehr Infos","","","","?id=1&layer=4&latitude=" + req.query.latitude + "&longitude=" + req.query.longitude + "&title=" + item.title + "&imageUrl=" + item.imageUrl,"","","",""))
                        item.items.push(new Item(2, "Navigiere dorthin" , "Navigiere dorthin","","","","?id=2&layer=4&latitude=" + req.query.latitude + "&longitude=" + req.query.longitude + "&title=" + item.title + "&imageUrl=" + item.imageUrl,"","","",""))
    
                        // An item can by categorized as station item, ...

                        if(
                            item.descriptionUrl.toLowerCase().includes("bahnhof") || 
                            item.descriptionUrl.toLowerCase().includes("station") ||
                            item.title.toLowerCase().includes("bahnhof") ||
                            item.title.toLowerCase().includes("station")
                            ){
                                item.category = "station"                             
                                item.id = y
                                item.url = "?id=" + y + "&layer=3&latitude=" + latitude + "&longitude=" + longitude + "&title=" + item.title + "&imageUrl=" + item.imageUrl + "&category=" + item.category
                                y++
                              
                                // define station-related additional operations 

                                item.items.push(new Item(3, "Zeige Abfahrplan" , "Zeige Abfahrplan","","","","?id=3&layer=4&latitude=" + req.query.latitude + "&longitude=" + req.query.longitude + "&title=" + item.title + "&imageUrl=" + item.imageUrl,"",""))
                                item.items.push(new Item(4, "Zeige Ankunftsplan" , "Zeige Ankunftsplan","","","","?id=4&layer=4&latitude=" + req.query.latitude + "&longitude=" + req.query.longitude + "&title=" + item.title + "&imageUrl=" + item.imageUrl,"",""))
                                
                                items[1].items.push(item)                                   
                            }else{

                                // ... non-station-item

                                item.url = "?id=" + i + "&layer=3&latitude=" + latitude + "&longitude=" + longitude + "&title=" + item.title + "&imageUrl=" + item.imageUrl + "&category=" + item.category
                                i++                                               
                                items[0].items.push(item)                          
                            }
                    }
                }
                res.render('index',{items:items[req.query.id].items, headline:headline, headlineImgUrl:null, layer:2})    
            }            
        })                      
    }

    // layer 3 offers operations on selected item

    if(req.query.layer == 3){
        
        headline = req.query.title
        
        let headlineImgUrl = req.query.imageUrl
        
        let cat = (req.query.category === "station") ? 1 : 0

        res.render('index',{items:items[cat].items[req.query.id].items, headline:headline, headlineImgUrl:headlineImgUrl,  layer:3})        
    }

    // 4th layer executes selected operations for specific wikipedia-result

    if(req.query.layer == 4){
        
        // get more info
        
        if(req.query.id == 1){            
            
            let url = "https://de.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&explaintext=1&titles=" + req.query.title;

            request({
                url: url,
                json: true
            }, function (error, response, body) {
            
                if (!error && response.statusCode === 200) {
    
                    var result = body.query.pages
                                  
                    let paragraphs = [];
                                        
                    for (const resu of Object.keys(result)) {
                        
                        

                        var paragraph = ((result[resu].extract).toString()).replace("===","==").split("==")//match(/([^\.!\?]+[\.!\?]+)|([^\.!\?]+$)/g)
                       
                        for (i = 0; i < paragraph.length; i++) { 


                            console.log("   " + paragraph[i])

                            var item = new Item();                  
                            
                            let description = paragraph[i].trim().length >= 5 ? paragraph[i].trim() : ""

                            if(description.substring(0,1) == "="){
                                description = description.substring(1, description.length).trim()
                            }
                            
                            item.description = description
                            
                            if(paragraph[i].trim().length > 10){
                                paragraphs.push(item)                                
                                //console.log("D" + i + ": '" + item.description +"'")                            
                            }   

                            if(item.description.length < 100)
                            item.aclass ="table-view-divider"
                            item.icon = ""
                        }
                    }       
                    res.render('index',{items:paragraphs, headline:req.query.title, headlineImgUrl:req.query.imageUrl,  layer:4})
                }             
            })            
        }
        if(req.query.id == 2){            
            //res.render('index',{items:null, headline:"Navigation gestartet", headlineImgUrl:req.query.imageUrl,  layer:4})
        }

        if(req.query.id == 3 || req.query.id == 4){            

            let ibnr = "";

            let destinations = []

            let url = "https://de.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=" + req.query.title + "&rvsection=0";

            request({
                url: url,
                json: true
            }, function (error, response, body) {
            
                if (!error && response.statusCode === 200) {
    
                    var result = body.query.pages
                                        
                    for (const r of Object.keys(result)) {
                        
                        for(var key in result[r].revisions) {
                            
                            var value = result[r].revisions[key];     

                            for(var ke in value) {
                                
                                var ibnrIndex = value[ke].indexOf("IBNR");
                                
                                ibnr = value[ke].substring(ibnrIndex + 7, ibnrIndex + 14)
                            }
                        }
                    }
                    
                    const fahrplan = require('fahrplan')('DBhackFrankfurt0316');
 
                    fahrplan.departure.find('Coesfeld')
                    .then(departures => {

                        departures.forEach(stop => destinations.push(new Item("","",stop.name + " " + stop.destination + " " + stop.platform + " " + stop.departure,"","","","","","","","")));
                        console.log(destinations.length)
                    
                        res.render('index',{items:destinations, headline:"Nächste Abfahrzeiten: " + req.query.title + " (IBNR:" + ibnr + ")", headlineImgUrl:req.query.imageUrl,  layer:3})

                      })
                }             
            })            
        }
    }        
})

app.post('/', function(req, res) {
    
    console.log("Neuen Searchstring '" + req.body.searchstring + "' entgegengenommen.");

    let headline = "Eine Wikipedia-Aktion wählen."

    let url = "https://de.wikipedia.org/w/api.php?action=opensearch&list=geosearch&search=" + req.body.searchstring + "&limit=10&namespace=0&format=json";

    items = []

    request({
        url: url,
        json: true
    }, function (error, response, body) {
        
        if (!error && response.statusCode === 200) {
                    
            if(body === undefined){
                
                console.log("body.query undefined")

            }else{

                var results = body

                let i = 1;

                for (const result of Object.keys(results)) {
                    
                    if(i === 2){

                        console.log(results[result].toString())

                        for (const treffer of results[result].toString().split(",")) {                    
                            var item = new Item();
                            item.id = i;                            
                            item.title = treffer
                            item.description = treffer
                            item.url = "?id=1&layer=4&latitude=" + req.query.latitude + "&longitude=" + req.query.longitude + "&title=" + item.title + "&imageUrl=" + item.imageUrl
                            items.push(item)
                            console.log(item.title)
                        }

                        console.log(body)
                        res.render('index',{items:items, headline:"Ergebnis", headlineImgUrl:"", layer:2})    
                    }
                    
                    i++
                }
            }
        }
    })
});   

var port = process.env.PORT || 3000;

var listener = app.listen(port, function() {
    console.log('Your app is listening on port ' + listener.address().port);
});




function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
  }



