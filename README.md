# COBI-Wiki

Thank you for being interested in COBI-Wiki.

## Idea

While cruising with your bike, COBI-Wiki displays interesting places nearby, using awesome Wikipedia-API. Everything is controllable with COBI-controller.

## Features

* Find interesting places nearby 
* Read Wikipedia-content about interesting places
* Navigate to the place of your choice by clicking it
* Search for stations nearby
* Display arrivals / departures of stations nearby for Deutsche Bahn


## Still interested?

[Click here for unready, far away from half-baked Sneak Preview](https://boiling-castle-47530.herokuapp.com/).

## License
COBI-Wiki is licensed und the terms of [GPL3](http://www.gnu.org/licenses/gpl-3.0.html). 



Have fun!

stbaeumer
