COBI.init('token');

let activeElement = 1;

let activeElementLat = "1"
let activeElementLon = "2"

navigator.geolocation.getCurrentPosition(function(position){
  
  activeElementLat = position.coords.latitude 
  activeElementLon = position.coords.longitude
  
  }, function () {
  // der zweite Funktionsteil wird ausgeführt, wenn keine
  //Positionsermittlung stattfinden konnte
})




document.getElementById('span' + activeElement).className = 'icon icon-star-filled'; 

var ul = document.getElementById("liste");

var items = ul.getElementsByTagName("a");

var descriptions = ul.getElementsByTagName("p");

let listLength = items.length

COBI.hub.externalInterfaceAction.subscribe(function(action) {  

  let el = document.getElementById('span' + activeElement);

  switch (action) {
        case "DOWN":                                        
        
          // If list's end isn't reached yet, this and the next item change star

          if(activeElement < listLength){
        
            if(document.getElementById("lat" + activeElement) != null){
              activeElementLat = document.getElementById("lat" + activeElement).innerText
            }
                          
            if(document.getElementById("lon" + activeElement) != null){
              activeElementLon = document.getElementById("lon" + activeElement).innerText
            }

            el.className=(el.className=='icon icon-star-filled')?'icon icon-star':(el.className=='icon icon-star-filled')?'icon icon-star':'icon icon-star-filled';
            
            activeElement++                

            let v = document.getElementById('span' + activeElement);
            v.className=(v.className=='icon icon-star-filled')?'icon icon-star':(v.className=='icon icon-star-filled')?'icon icon-star':'icon icon-star-filled';
            el.scrollIntoView({behavior: "smooth", block: "start", inline: "start"})
          }
                        
          return

        case "UP":   

          // If list's top isn't reached yet, this and the before item change star

          if(activeElement > 1){

            if(document.getElementById("lat" + activeElement) != null){
              activeElementLat = document.getElementById("lat" + activeElement).innerText
            }

            if(document.getElementById("lon" + activeElement) != null){
              activeElementLon = document.getElementById("lon" + activeElement).innerText  
            }
                       
            el.className=(el.className=='icon icon-star-filled')?'icon icon-star':(el.className=='icon icon-star-filled')?'icon icon-star':'icon icon-star-filled';
            
            activeElement--                

            let v = document.getElementById('span' + activeElement);
            
            v.className=(v.className=='icon icon-star-filled')?'icon icon-star':(v.className=='icon icon-star-filled')?'icon icon-star':'icon icon-star-filled';
            
            let vv = document.getElementById('span' + (activeElement - 1));

            if(vv != null){
              vv.scrollIntoView({behavior: "smooth", block: "start", inline: "start"})    
            }                     
          }
          return
        case "SELECT":          
          document.getElementById(activeElement).click();          
          return
    }})

COBI.mobile.location.subscribe(function(location) {
  lastLocation = location;
  latitude = lastLocation.coordinate.latitude
  longitude = lastLocation.coordinate.longitude    
  
  if(ul.title == 1){
    for (var i = 0; i < items.length; ++i) {    
      items[i].href = items[i].href.replace(/latitude=[\s\S]*?&longitude/, 'latitude=' + latitude + '&longitude')    
      items[i].href = items[i].href.replace(/longitude=[\s\S]*?&layer/, 'longitude=' + longitude + '&layer')    
    }
  }

  if(ul.title == 2){
    for (var i = 0; i < items.length; ++i) {    

      let lat = document.getElementById("lat" + i).innerText        
      
      let lon = document.getElementById("lon" + i).innerText
  
      var R = 6371; // Radius of the earth in km
      var dLat = deg2rad(lat-latitude);  // deg2rad below
      var dLon = deg2rad(lon-longitude); 
      var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat)) * Math.cos(deg2rad(latitude)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ; 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c; // Distance in km
  
      descriptions[i].innerHTML = descriptions[i].innerHTML.replace(/( ~[\s\S]*? km )/, ' ~' + d.toFixed(3) + ' km ')        
    }  
  }
    
  document.getElementById("coordinates").innerHTML = "Breitengrad: " + latitude + ", Längengrad: " + longitude
});

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function starteNavigation(name){
  if(name === "Navigiere dorthin"){

    if(activeElementLat == ""){
      if(document.getElementById("lat" + activeElement) != null){

        console.log("starteNavigation pressed: " + activeElementLat)

        activeElementLat = document.getElementById("lat" + activeElement).innerText

        console.log("starteNavigation pressed: " + activeElementLat)
      }
    }

    if(activeElementLon == ""){
      if(document.getElementById("lon" + activeElement) != null){
        activeElementLon = document.getElementById("lon" + activeElement).innerText
      }
    }

    document.getElementById("footer").innerHTML = "Navigation gestartet. Zielkoordinaten: " + activeElementLat + ", " + activeElementLon

    COBI.navigationService.control.write({
      'action': 'START', 
      'destination': {'latitude': activeElementLat,'longitude': activeElementLon}
    });    
  }
}

